<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Helper\Helper;

use App\Models\Status;

class StatusController extends Controller
{
    protected $data_req = [
        'name' => 'required'
    ];

    public function index(Request $request)
    {
        try {
            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => Helper::queryParam($request->all(), 'App\Models\Status')->orderBy('updated_at', 'DESC')->get()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function detail($id_status)
    {
        try {

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => Status::where('id_status', $id_status)->first()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function create(Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            $post = Status::create(array_merge($input, [
                'created_by' => $request->user()->username,
                'id_status' => Status::orderBy('id_status', 'desc')->first()->id_status + 1
            ]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $post
            ], 200);

        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update($id_status, Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            Status::where('id_status', $id_status)->update(array_merge($input, ['updated_by' => $request->user()->username]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete($id_status)
    {
        try {
            $data = Status::where('id_status', $id_status)->first();
            Status::where('id_status', $id_status)->delete();

            return response()->json([
                'status'  => $data ? 200 : 500,
                'message' => $data ? 'delete success' : 'delete failed',
                'data'    => $data
            ], $data ? 200 : 500);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
