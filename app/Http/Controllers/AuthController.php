<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\User_Detail;
use App\Models\OauthAccessTokens;

class AuthController extends Controller
{
    /**
     * Register
     */
    public function register(Request $request)
    {
        $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

        /** Validate input */
        $validator = Validator::make($request->all(), [
            'username'          => 'required',
            'password'          => 'required',
            'confirm_password'  => 'required|same:password',
        ]);

        /** chack validate input */
        if ($validator->fails()) {
            return response()->json([
                'status'    => 501,
                'message'   => $validator->errors()
            ], 501);
        }

        /** check exist username */
        $exist_user = User::where('username', '=', $request->username)->first();

        if ($exist_user) {
            return response()->json([
                'status'  => 501,
                'message' => 'username already registered'
            ], 501);
        }

        try {

            /** post user detail */
            $get_user_detail_id = User_Detail::orderBy('id_user', 'desc')->first();
            $set_user_detail_id = !empty($get_user_detail_id) ? ($get_user_detail_id->id_user + 1) : 1;

            User_Detail::create(array_merge($input, [
                'id_user' => $set_user_detail_id,
                'id_user_type' => '2',
                'name' => $input['username'],
                'created_by' => 'new member',
            ]));

            /** post user auth */
            $get_user_id = User::orderBy('id', 'desc')->first();
            $set_user_id = !empty($get_user_id) ? ($get_user_id->id + 1) : 1;

            $input['password'] = bcrypt($input['password']);
            User::create(array_merge($input, [
                'id' => $set_user_id,
                'name' => $input['username'],
                'id_user' => $set_user_detail_id,
                'id_user_type' => '2'
            ]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->except(['password', 'confirm_password'])
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }


    /**
     * Login
     */
    public function login(Request $request)
    {
        $credential = request(['username', 'password']);

        if (!Auth::attempt($credential)) {
            return response()->json([
                'status'  => 401,
                'message' => 'username and password doesn\'t match'
            ], 401);
        }

        $user        = $request->user();
        $tokenResult = $user->createToken($request->username);
        $token       = $tokenResult->token;

        $token->save();

        return response()->json([
            'status'  => 200,
            'message' => 'Success',
            'data'    => [
                'detail_user'  => User_Detail::where('id_user', $user->id_user)
                    ->with('user_type:id_user_type,name')
                    ->select(
                        'id_user_type',
                        'name',
                    )->first(),
                'access_token' => $tokenResult->accessToken,
                'expired_token_date' => OauthAccessTokens::where('id', $tokenResult->token->id)->select('expires_at')->get()[0]->expires_at,
                'id_user_type' => $user->id_user_type,
            ],
        ], 200);
    }


    /**
     * Function Logout
     */
    public function logout(Request $request)
    {
        try {

            $query = $request->user()->token()->delete();
            return response()->json([
                'status'  => 200,
                'message' => 'Logout Success',
                'data'    => $query
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function remove_old_token()
    {
        $response = [];
        $today = date("Y-m-d h:m:s");
        $data = OauthAccessTokens::whereDate('expires_at', '<', $today)->get();

        foreach ($data as $key => $value) {
            $request = OauthAccessTokens::where('created_at', $value->created_at)->delete();
            if ($request) {
                $response[] = $value->id;
            } else {
                return response()->json([
                    'status'  => 501,
                    'message' => 'Delete Old Token Failed at id = ' . $value->id
                ], 501);
            }
        }

        return response()->json([
            'status'  => 200,
            'message' => 'Delete Old Token Success'
        ], 200);
    }
}
