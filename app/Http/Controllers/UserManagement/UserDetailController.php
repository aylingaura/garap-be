<?php

namespace App\Http\Controllers\UserManagement;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Helper\Helper;

use App\Models\User_Detail;
use App\Models\User;

class UserDetailController extends Controller
{

    public function index(Request $request)
    {
        try {
            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => Helper::queryParam($request->all(), 'App\Models\User')
                    ->orderBy('updated_at', 'DESC')
                    ->with('user_detail:id_user,name,email,phone')
                    ->with('user_type:id_user_type,name')
                    ->select(
                        'id',
                        'name',
                        'username',
                        'created_at',
                        'updated_at',
                        'id_user',
                        'id_user_type',
                    )
                    ->get()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function detail(Request $request, $id_user)
    {
        try {

            $id_user_detail = User::where('id', $id_user)->first()->id_user;
            /** get user detail */
            $image = User_Detail::where('id_user', $id_user_detail)->first();

            /** if user detail exist get data photo */
            if ($image) {
                /** if user exist and photo not null */
                $image = $image->photo ? $image->photo : 'no img';
            } else {
                $image = 'no user no image';
            }

            $base64_image = null;
            $filepath = public_path() . '/images/' . $image;

            if (file_exists($filepath)) {

                $filetype = pathinfo($filepath, PATHINFO_EXTENSION);

                if ($filetype === 'svg') {
                    $filetype .= '+xml';
                }

                $get_img = file_get_contents($filepath);
                $base64_image = 'data:image/' . $filetype . ';base64,' . base64_encode($get_img);
            }

            $data = Helper::queryParam($request->all(), 'App\Models\User')
                ->orderBy('updated_at', 'DESC')
                ->with('user_detail')
                ->with('user_type')
                ->where('id', $id_user)
                ->first();

            $data['photo'] = $base64_image;

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function create(Request $request)
    {
        $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

        /** Validate input */
        $validator = Validator::make($request->all(), [
            'username'          => 'required',
            'password'          => 'required',
            'id_user_type'      => 'required',
            'confirm_password'  => 'required|same:password',
        ]);

        /** chack validate input */
        if ($validator->fails()) {
            return response()->json([
                'status'    => 501,
                'message'   => $validator->errors()
            ], 501);
        }

        /** check exist username */
        $exist_user = User::where('username', '=', $request->username)->first();

        if ($exist_user) {
            return response()->json([
                'status'  => 501,
                'message' => [
                    'username' => ['username already registered']
                ]
            ], 501);
        }

        try {

            /** post user detail */
            $get_user_detail_id = User_Detail::orderBy('id_user', 'desc')->first();
            $set_user_detail_id = !empty($get_user_detail_id) ? ($get_user_detail_id->id_user + 1) : 1;

            User_Detail::create(array_merge($input, [
                'id_user' => $set_user_detail_id,
                'name' => $input['username'],
                'created_by' => $request->user()->username,
            ]));

            /** post user auth */
            $get_user_id = User::orderBy('id', 'desc')->first();
            $set_user_id = !empty($get_user_id) ? ($get_user_id->id + 1) : 1;

            $input['password'] = bcrypt($input['password']);
            User::create(array_merge($input, [
                'id' => $set_user_id,
                'name' => $input['username'],
                'id_user' => $set_user_detail_id
            ]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->except(['password', 'confirm_password'])
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update_profile($id_user, Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), [
                'photo' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'message'   => $validator->errors()
                ], 501);
            }

            $file = $request->file('photo');

            $file_name = $request->user()->username . '-' . date("YmdHis") . '-' . $request->file('photo')->getClientOriginalName();
            $path = public_path() . '/images';

            $input = array_merge($input, [
                'photo' => $file_name
            ]);

            /** store data */
            User_Detail::where('id_user', $id_user)->update(array_merge($input, ['updated_by' => $request->user()->username]));

            /** store file to public folder */
            $file->move($path, $file_name);

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function reset_password($id_user, Request $request)
    {
        try {
            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), [
                'password'          => 'required',
                'confirm_password'  => 'required|same:password',
            ]);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'message'   => $validator->errors()
                ], 501);
            }

            $data = User::where('id', $id_user)->update(['password' => bcrypt($input['password'])]);

            return response()->json([
                'status'  => 200,
                'message' => 'change password success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function change_username($id_user, Request $request)
    {
        try {
            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), [
                'username' => 'required',
            ]);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'message'   => $validator->errors()
                ], 501);
            }

            /** check exist username */
            $exist_user = User::where('username', '=', $input['username'])->first();

            if ($exist_user) {
                return response()->json([
                    'status'  => 501,
                    'message' => 'username already registered'
                ], 501);
            }

            $data = User::where('id', $id_user)->update([
                'name' => $input['username'],
                'username' => $input['username'],
            ]);

            return response()->json([
                'status'  => 200,
                'message' => 'change username success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete($id)
    {
        try {
            $id_user = User::where('id', $id)->first()->id_user;

            $delete_user_detail = User_Detail::where('id_user', $id_user)->delete();
            $delete_user = User::where('id', $id)->delete();

            return response()->json([
                'status'  => 200,
                'message' => 'delete success',
                'data'    => [
                    'delete_user' => $delete_user,
                    'delete_user_detail' => $delete_user_detail
                ]
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
