<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_Type extends Model
{
    use HasFactory;
    protected $table = 'm_user_type';
    protected $primaryKey = 'id_user_type';
    protected $fillable = [
        'id_user_type',
        'name',
        'description',
        'status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
}
