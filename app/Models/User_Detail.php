<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_Detail extends Model
{
    use HasFactory;
    protected $table = 'user_detail';
    protected $primaryKey = 'id_user';
    protected $fillable = [
        'id_user',
        'id_user_type',
        'name',
        'pob',
        'dob',
        'id_province',
        'id_city',
        'id_district',
        'id_village',
        'education',
        'email',
        'phone',
        'photo',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    function user_type()
    {
        return $this->hasOne(User_Type::class, 'id_user_type', 'id_user_type');
    }
}
