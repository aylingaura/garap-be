<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    use HasFactory;
    protected $table = 'projects';
    protected $primaryKey = 'id_projects';
    protected $fillable = [
        'id_user',
        'name',
        'description',
        'start_date',
        'end_date',
        'id_status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    function user_detail()
    {
        return $this->hasOne(User_Detail::class, 'id_user', 'id_user');
    }

    function status()
    {
        return $this->hasOne(Status::class, 'id_status', 'id_status');
    }
}
