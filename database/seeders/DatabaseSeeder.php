<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\User_Type;
use App\Models\User_Detail;
use App\Models\Status;

use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'admin',
                'username' => 'admin',
                'id_user' => '0',
                'id_user_type' => '1',
                'password' => Hash::make('pass'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'teacher',
                'username' => 'teacher',
                'id_user' => '1',
                'id_user_type' => '2',
                'password' => Hash::make('pass'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'student',
                'username' => 'student',
                'id_user' => '4',
                'id_user_type' => '3',
                'password' => Hash::make('pass'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];

        $user_type = [
            [
                'id_user_type' => '1',
                'name' => 'Admin',
                'description' => 'Admin',
                'created_by' => 'Admin',
                'status' => true,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_user_type' => '2',
                'name' => 'Teacher',
                'description' => 'Teacher',
                'created_by' => 'Admin',
                'status' => true,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_user_type' => '3',
                'name' => 'Student',
                'description' => 'Student',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];

        $user_detail = [
            [
                'id_user' => '1',
                'name' => 'Admin',
                'id_user_type' => '1',
                'pob' => 'Yogjakarta',
                'dob' => '22-09-1990',
                'id_province' => '0',
                'id_city' => '0',
                'id_district' => '0',
                'id_village' => '0',
                'education' => 'S1',
                'email' => '',
                'phone' => '',
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ];

        $status = [
            [
                'id_status' => '1',
                'name' => 'Backlog',
                'description' => 'Backlog',
                'created_by' => 'Admin',
                'status' => true,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_status' => '2',
                'name' => 'On Progress',
                'description' => 'On Progress',
                'created_by' => 'Admin',
                'status' => true,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_status' => '3',
                'name' => 'Done',
                'description' => 'Done',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_status' => '4',
                'name' => 'Close',
                'description' => 'Close',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];

        // Send Data Seed
        // User::insert($user);
        // User_Type::insert($user_type);
        User_Detail::insert($user_detail);
        // Status::insert($status);
    }
}
