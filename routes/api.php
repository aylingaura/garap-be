<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Http\Controllers\AuthController;

use App\Http\Controllers\UserManagement\UserDetailController;

use App\Http\Controllers\Master\UserTypeController;
use App\Http\Controllers\Master\StatusController;

use App\Http\Controllers\Projects;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);

    Route::group(['middleware' => 'auth:api'], function () {

        Route::get('logout', [AuthController::class, 'logout']);
        Route::get('remove-old-token', [AuthController::class, 'remove_old_token']);

        # USER MANAGEMENT
        Route::get('user-management/user', [UserDetailController::class, 'index']);
        Route::get('user-management/user/{id_user}', [UserDetailController::class, 'detail']);
        Route::post('user-management/user', [UserDetailController::class, 'create']);
        Route::post('user-management/user/update-profile/{id_user}', [UserDetailController::class, 'update_profile']);
        Route::put('user-management/user/reset-password/{id_user}', [UserDetailController::class, 'reset_password']);
        Route::put('user-management/user/change-username/{id_user}', [UserDetailController::class, 'change_username']);
        Route::delete('user-management/user/{id_user}', [UserDetailController::class, 'delete']);

        # MASTER
        Route::get('master/user-type', [UserTypeController::class, 'index']);
        Route::get('master/user-type/{id_user_type}', [UserTypeController::class, 'detail']);
        Route::post('master/user-type', [UserTypeController::class, 'create']);
        Route::put('master/user-type/{id_user_type}', [UserTypeController::class, 'update']);
        Route::delete('master/user-type/{id_user_type}', [UserTypeController::class, 'delete']);

        Route::get('master/status', [StatusController::class, 'index']);
        Route::get('master/status/{id_status}', [StatusController::class, 'detail']);
        Route::post('master/status', [StatusController::class, 'create']);
        Route::put('master/status/{id_status}', [StatusController::class, 'update']);
        Route::delete('master/status/{id_status}', [StatusController::class, 'delete']);

        # PROJECTS
        Route::get('projects/{id_user}', [Projects::class, 'index']);
        Route::post('projects', [Projects::class, 'create']);
        Route::put('projects/{id_projects}', [Projects::class, 'update']);
        Route::delete('projects/{id_projects}', [Projects::class, 'delete']);

        # TASK
        Route::get('task/{id_projects}/{id_user}', [Projects::class, 'index']);
        Route::post('task', [Projects::class, 'create']);
        Route::put('task/{id_task}', [Projects::class, 'update']);
        Route::put('task/status/{id_task}', [Projects::class, 'update_status']);
        Route::delete('task/{id_task}', [Projects::class, 'delete']);

        # LOG
        Route::get('log/{id_projects}/{id_task}', [Projects::class, 'index']);
        Route::post('log', [Projects::class, 'create']);
        Route::post('log/{id_log}', [Projects::class, 'update']);
    });
});
